const asynclib = require(`asyncawait/async`);
const awaitlib = require(`asyncawait/await`);
const bluebird = require(`bluebird`);

const apiKey = `key-9fd06ddbdd0532ba9d72469b3688a067`;
const domain = `sandbox59aaab0483b4486a847901a7e4569cb5.mailgun.org`;
const mailgun = require(`mailgun-js`)({ apiKey, domain });

exports.routes = asynclib((app, logger) => {
    app.get(`/`, asynclib((req, res) => {
        try {
            let body = `<b>to</b> email not specified`;
            const messagesAsync = bluebird.promisifyAll(mailgun.messages());
            const data = {
                from: `Sears Wish Book <wishbook@sears.ca>`,
                to: ``,
                subject: `WishList`,
                cc: [],
                html: `Here is the wishlist`,
            };

            if (req.query.subject) {
                data.subject = req.query.subject;
            }

            if (req.query.body) {
                data.html = req.query.body;
            }

            if (req.query.cc) {
                data.cc = req.query.cc.split(`|`);
            }

            if (req.query.to) {
                data.to = req.query.to;
                body = awaitlib(messagesAsync.sendAsync(data));
            }

            res.send(body);
        } catch (error) {
            logger.error(`${error.stack}`);
            res.send(`${error.message}`);
        }
    }));
});
