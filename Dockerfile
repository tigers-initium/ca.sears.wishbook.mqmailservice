FROM node:latest

RUN npm install -g nodemon

WORKDIR /app

RUN npm install

ENV NODE_ENV development

CMD npm start
